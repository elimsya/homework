package main

import (
	"errors"
	"io"
	"log"
	"net"
	"net/http"
	"runtime"
	"strings"
)

/*
作业描述
1.接收客户端 request，并将 request 中带的 header 写入 response header
2.读取当前系统的环境变量中的 VERSION 配置，并写入 response header
3.Server 端记录访问日志包括客户端 IP，HTTP 返回码，输出到 server 端的标准输出
4.当访问 localhost/healthz 时，应返回200
*/

func main() {
	http.HandleFunc("/", requestHandle)
	http.HandleFunc("/healthz", healthz)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}

func healthz(responseWriter http.ResponseWriter, request *http.Request) {
	statusCode := 200
	_, err := io.WriteString(responseWriter, "200 \n")
	if err != nil {
		statusCode = 500
		log.Printf("请求异常:%s", err)
	}
	logVisitor(request, statusCode)
}

func requestHandle(responseWriter http.ResponseWriter, request *http.Request) {
	defer logVisitor(request, 200)
	//将版本号信息写入到响应Header中
	addVersion2ResponseHeader(responseWriter)
	//将请求中的header放入响应的header中
	addRequestHeader2ResponseHeader(responseWriter, request)
}

func logVisitor(request *http.Request, statusCode int) {
	clientAddr, err := getClientIp(request)
	if err != nil {
		log.Printf("statusCode:%v, %s \n", 500., err)
		return
	}
	log.Printf("from: %s, statusCode:%d \n", clientAddr, statusCode)
}

func getClientIp(r *http.Request) (string, error) {
	ip := r.Header.Get("X-Real-IP")
	if net.ParseIP(ip) != nil {
		return ip, nil
	}

	ip = r.Header.Get("X-Forward-For")
	for _, i := range strings.Split(ip, ",") {
		if net.ParseIP(i) != nil {
			return i, nil
		}
	}

	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return "", err
	}

	if strings.Contains(ip, "::1") {
		ip = "127.0.0.1"
	}

	if net.ParseIP(ip) != nil {
		return ip, nil
	}

	return "", errors.New("no valid ip found")
}

func addRequestHeader2ResponseHeader(writer http.ResponseWriter, request *http.Request) {
	for k, v := range request.Header {
		writer.Header().Add(k, strings.Join(v, ","))
	}
}

func addVersion2ResponseHeader(responseWriter http.ResponseWriter) {
	version := runtime.Version()
	responseWriter.Header().Add("VERSION", version)
}
